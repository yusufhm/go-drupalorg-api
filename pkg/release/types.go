package release

import (
	"encoding/xml"

	"gitlab.com/yusufhm/go-drupalorg-api/pkg/file"
	"gitlab.com/yusufhm/go-drupalorg-api/pkg/term"
)

const LatestReleaseConstraint string = "~10"

type Releases struct {
	XMLName  xml.Name  `xml:"releases"`
	Releases []Release `xml:"release"`
}

type Release struct {
	XMLName           xml.Name   `xml:"release"`
	Name              string     `xml:"name"`
	Version           string     `xml:"version"`
	Tag               string     `xml:"tag"`
	Status            string     `xml:"status"`
	ReleaseLink       string     `xml:"release_link"`
	DownloadLink      string     `xml:"download_link"`
	Date              uint64     `xml:"date"`
	Files             file.Files `xml:"files"`
	Terms             term.Terms `xml:"terms"`
	Security          string     `xml:"security"`
	CoreCompatibility string     `xml:"core_compatibility"`
}
