package file_test

import (
	"encoding/xml"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yusufhm/go-drupalorg-api/pkg/file"
)

func TestFileTypes(t *testing.T) {
	assert := assert.New(t)

	xmlData := []byte(`
<files>
	<file>
		<url>https://ftp.drupal.org/files/projects/drupal-10.0.3.tar.gz</url>
		<archive_type>tar.gz</archive_type>
		<md5>6a0eb164c5738dcbe102d34229e9b8e7</md5>
		<size>17418116</size>
		<filedate>1675276709</filedate>
	</file>
	<file>
		<url>https://ftp.drupal.org/files/projects/drupal-10.0.3.zip</url>
		<archive_type>zip</archive_type>
		<md5>c1568866c28b28d1b47f0d2a5d3afe55</md5>
		<size>28795129</size>
		<filedate>1675276709</filedate>
	</file>
</files>
`)
	var files file.Files
	err := xml.Unmarshal(xmlData, &files)
	assert.NoError(err)

	assert.Len(files.Files, 2)
	assert.EqualValues(file.Files{
		XMLName: xml.Name{Local: "files"},
		Files: []file.File{
			{
				XMLName:     xml.Name{Local: "file"},
				Url:         "https://ftp.drupal.org/files/projects/drupal-10.0.3.tar.gz",
				ArchiveType: "tar.gz",
				Md5:         "6a0eb164c5738dcbe102d34229e9b8e7",
				Size:        17418116,
				Filedate:    1675276709,
			},
			{
				XMLName:     xml.Name{Local: "file"},
				Url:         "https://ftp.drupal.org/files/projects/drupal-10.0.3.zip",
				ArchiveType: "zip",
				Md5:         "c1568866c28b28d1b47f0d2a5d3afe55",
				Size:        28795129,
				Filedate:    1675276709,
			},
		},
	}, files)
}
