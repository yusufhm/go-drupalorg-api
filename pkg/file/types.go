package file

import "encoding/xml"

type Files struct {
	XMLName xml.Name `xml:"files"`
	Files   []File   `xml:"file"`
}

type File struct {
	XMLName     xml.Name `xml:"file"`
	Url         string   `xml:"url"`
	ArchiveType string   `xml:"archive_type"`
	Md5         string   `xml:"md5"`
	Size        uint64   `xml:"size"`
	Filedate    uint64   `xml:"filedate"`
}
