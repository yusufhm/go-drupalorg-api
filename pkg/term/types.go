package term

import "encoding/xml"

type Terms struct {
	XMLName xml.Name `xml:"terms"`
	Terms   []Term   `xml:"term"`
}

type Term struct {
	XMLName xml.Name `xml:"term"`
	Name    string   `xml:"name"`
	Value   string   `xml:"value"`
}
