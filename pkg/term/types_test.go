package term_test

import (
	"encoding/xml"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yusufhm/go-drupalorg-api/pkg/term"
)

func TestTermTypes(t *testing.T) {
	assert := assert.New(t)

	xmlData := []byte(`
<terms>
	<term>
		<name>Projects</name>
		<value>Drupal core</value>
	</term>
	<term>
		<name>Maintenance status</name>
		<value>Actively maintained</value>
	</term>
	<term>
		<name>Development status</name>
		<value>Under active development</value>
	</term>
</terms>
`)
	var terms term.Terms
	err := xml.Unmarshal(xmlData, &terms)
	assert.NoError(err)

	assert.Len(terms.Terms, 3)
	assert.EqualValues(term.Terms{
		XMLName: xml.Name{Local: "terms"},
		Terms: []term.Term{
			{
				XMLName: xml.Name{Local: "term"},
				Name:    "Projects",
				Value:   "Drupal core",
			},
			{
				XMLName: xml.Name{Local: "term"},
				Name:    "Maintenance status",
				Value:   "Actively maintained",
			},
			{
				XMLName: xml.Name{Local: "term"},
				Name:    "Development status",
				Value:   "Under active development",
			},
		},
	}, terms)
}
