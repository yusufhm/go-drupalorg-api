package project

import (
	"encoding/xml"

	"gitlab.com/yusufhm/go-drupalorg-api/pkg/release"
	"gitlab.com/yusufhm/go-drupalorg-api/pkg/term"
)

type Type string

const (
	ProjectTypeCore         Type = "project_core"
	ProjectTypeDistribution Type = "project_distribution"
	ProjectTypeDrupalOrg    Type = "project_drupalorg"
	ProjectTypeModule       Type = "project_module"
	ProjectTypeTheme        Type = "project_theme"
	ProjectTypeThemeEngine  Type = "project_theme_engine"
	ProjectTypeTranslation  Type = "project_translation"
)

type Status string

const (
	ProjectStatusPublished   Status = "published"
	ProjectStatusUnsupported Status = "unsupported"
	ProjectStatusUnpublished Status = "unpublished"
)

const (
	ProjectTermProjects          string = "Projects"
	ProjectTermMaintenanceStatus string = "Maintenance status"
	ProjectTermDevelopmentStatus string = "Development status"
	ProjectTermModuleCategories  string = "Module categories"
)

type Project struct {
	XMLName           xml.Name         `xml:"project"`
	Title             string           `xml:"title"`
	ShortName         string           `xml:"short_name"`
	Type              Type             `xml:"type"`
	SupportedBranches string           `xml:"supported_branches"`
	ProjectStatus     Status           `xml:"project_status"`
	Link              string           `xml:"link"`
	Terms             term.Terms       `xml:"terms"`
	Releases          release.Releases `xml:"releases"`
}
