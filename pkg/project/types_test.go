package project_test

import (
	"encoding/xml"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yusufhm/go-drupalorg-api/pkg/project"
	"gitlab.com/yusufhm/go-drupalorg-api/pkg/release"
	"gitlab.com/yusufhm/go-drupalorg-api/pkg/term"
)

func TestProjectTypes(t *testing.T) {
	assert := assert.New(t)

	xmlData := []byte(`
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
	<title>Drupal core</title>
	<short_name>drupal</short_name>
	<dc:creator>Drupal</dc:creator>
	<type>project_core</type>
	<supported_branches>10.0.,10.1.,9.4.,9.5.</supported_branches>
	<project_status>published</project_status>
	<link>https://www.drupal.org/project/drupal</link>
	<terms>
		<term>
			<name>Projects</name>
			<value>Drupal core</value>
		</term>
	</terms>
	<releases>
		<release>
			<name>drupal 10.0.3</name>
			<version>10.0.3</version>
			<tag>10.0.3</tag>
			<status>published</status>
		</release>
	</releases>
</project>
`)
	var proj project.Project
	err := xml.Unmarshal(xmlData, &proj)
	assert.NoError(err)

	assert.Len(proj.Terms.Terms, 1)
	assert.Len(proj.Releases.Releases, 1)
	assert.EqualValues(project.Project{
		XMLName:           xml.Name{Local: "project"},
		Title:             "Drupal core",
		ShortName:         "drupal",
		Type:              "project_core",
		SupportedBranches: "10.0.,10.1.,9.4.,9.5.",
		ProjectStatus:     "published",
		Link:              "https://www.drupal.org/project/drupal",
		Terms: term.Terms{
			XMLName: xml.Name{Local: "terms"},
			Terms: []term.Term{
				{
					XMLName: xml.Name{Local: "term"},
					Name:    "Projects",
					Value:   "Drupal core",
				},
			},
		},
		Releases: release.Releases{
			XMLName: xml.Name{Local: "releases"},
			Releases: []release.Release{
				{
					XMLName: xml.Name{Local: "release"},
					Name:    "drupal 10.0.3",
					Version: "10.0.3",
					Tag:     "10.0.3",
					Status:  "published",
				},
			},
		},
	}, proj)
}
