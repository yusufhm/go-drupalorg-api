package project

import (
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/Masterminds/semver"
)

func (p *Project) GetLatestStableRelease(constraint string) (string, error) {
	c, err := semver.NewConstraint(constraint)
	if err != nil {
		return "", fmt.Errorf("constraint is not parsable: %s", constraint)
	}
	for _, r := range p.Releases.Releases {
		v, err := semver.NewVersion(r.Version)
		if err != nil {
			log.Fatalf("version is not parsable: %s", r.Version)
		}
		if c.Check(v) {
			return r.Version, nil
		}
	}
	return "", nil
}

// FetchProjectReleases fetches a project's releases from the
// drupal.org update status xml.
//
// See https://www.drupal.org/drupalorg/docs/apis/update-status-xml
func FetchProjectReleases(project string) (Project, error) {
	url := fmt.Sprintf("https://updates.drupal.org/release-history/%s/current", project)
	resp, err := http.Get(url)
	if err != nil {
		return Project{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return Project{}, fmt.Errorf("status error: %v", resp.StatusCode)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return Project{}, fmt.Errorf("read body: %v", err)
	}

	var prj Project
	err = xml.Unmarshal(data, &prj)
	if err != nil {
		return Project{}, fmt.Errorf("unmarshal: %v", err)
	}
	return prj, nil
}
